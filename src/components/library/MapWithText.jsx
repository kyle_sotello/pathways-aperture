import React from 'react';

import {
  BLACK,
  PURPLE,
  TRANSPARENT,
  WHITE,
} from '@apertures/shared/dist/utilities/colors';
import Map from '@apertures/vis-map';

function MapWithText() {
  return (
    <Map
      overlayText="Olive"
      theme={{
        background: PURPLE.PRIMARY,
        outline: BLACK.PRIMARY,
        shadow: TRANSPARENT,
        text: WHITE.PRIMARY,
      }}
      values={[]}
    />
  );
}

export default MapWithText;
