import React, { useEffect, useState } from 'react';
import Shamrock from '@apertures/vis-shamrock';

function generateCpa() {
  return {
    quadrant: Math.floor(Math.random() * 4),
    size: Math.floor(Math.random() * 3) + 1,
    group: Math.floor(Math.random() * 5),
  };
}

const datasets = [
  new Array(4).fill('').map(generateCpa),
  new Array(4).fill('').map(generateCpa),
  new Array(27).fill('').map(generateCpa),
  new Array(10).fill('').map(generateCpa),
  new Array(51).fill('').map(generateCpa),
  new Array(89).fill('').map(generateCpa),
  new Array(36).fill('').map(generateCpa),
];

function ShamrockExample() {
  const [values, setValues] = useState(0);
  const [activeLeaves, setActiveLeaves] = useState([]);
  const handleLeafClick = (idx) => {
    setActiveLeaves((prev) => {
      if (prev.includes(idx)) {
        return prev.filter((item) => item !== idx);
      }
      return [...prev, idx];
    });
  };

  useEffect(() => {
    const intervalHandle = setInterval(() => {
      setValues((prev) => (prev + 1) % datasets.length);
    }, 10000);

    return () => clearInterval(intervalHandle);
  }, []);

  return (
    <div style={{ backgroundColor: '#651fff', padding: 30 }}>
      <Shamrock
        activeLeaves={activeLeaves}
        handleLeafClick={handleLeafClick}
        labels={{ primary: 'speed', secondary: 'impact' }}
        theme={{ outline: 'orange', text: '#000' }}
        values={datasets[values]}
        width={500}
      />
    </div>
  );
}

export default ShamrockExample;
