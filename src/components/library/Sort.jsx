import React, { useState } from 'react';
import Sort from '@apertures/shared/dist/components/sort';

const SORT_OPTIONS = [
  {
    key: '1',
    label: 'Option 1',
  },
  {
    key: '2',
    label: 'Option 2',
  },
  {
    key: '3',
    label: 'Option 3',
  },
];

function SortExample() {
  const [selectedSort, setSelectedSort] = useState('2');

  return (
    <Sort
      handleSortChange={setSelectedSort}
      options={SORT_OPTIONS}
      selectedSort={selectedSort}
    />
  );
}

export default SortExample;
