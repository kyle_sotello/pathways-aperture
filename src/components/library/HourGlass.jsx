import React from 'react';
import Flexbox from 'flexbox-react';
import Hourglass from '@apertures/vis-hourglass';
import {
  BLUE,
  GREEN,
  ORANGE,
  PURPLE,
} from '@apertures/shared/dist/utilities/colors';

function HourglassExample() {
  const values = [
    [600000, 200000, 200000],
    [2000000, 1000000, 1000000],
  ];

  return (
    <Flexbox alignItems="center" flexDirection="column">
      <Hourglass
        hourglassLabels={['some top label', 'some bottom label']}
        values={values}
        waveLabels={['Revenue Cycle', 'Clinical', 'Shared Services']}
      />
      <Hourglass
        hourglassLabels={['what a beautiful', 'hourglass']}
        size={600}
        theme={{
          colors: [BLUE.LIGHT, ORANGE.PRIMARY, BLUE.PRIMARY],
          outline: GREEN.MINT,
          text: PURPLE.PERIWINKLE,
        }}
        values={values}
        waveLabels={['BLUE.LIGHT', 'ORANGE.PRIMARY', 'BLUE.PRIMARY']}
      />
      <Hourglass
        hourglassLabels={['some top label', 'some bottom label']}
        size={800}
        values={values}
        waveLabels={['Revenue Cycle', 'Clinical', 'Shared Services']}
      />
    </Flexbox>
  );
}

export default HourglassExample;
