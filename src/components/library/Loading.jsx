/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import Loading from '@apertures/shared/dist/components/loading';

function LoadingExample() {
  const divProps = {
    className: 'loading-target',
    style: { height: 100, width: 100 },
  };

  return (
    <>
      <div {...divProps}>
        <Loading />
      </div>
      <div {...divProps}>
        <Loading theme={{ background: '#0f0', colors: ['#008'] }} />
      </div>
    </>
  );
}

export default LoadingExample;
