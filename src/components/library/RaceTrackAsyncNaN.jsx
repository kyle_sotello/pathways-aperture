import React, { useEffect, useState } from 'react';
import Racetrack from '@apertures/vis-olive-racetrack';

function RacetrackAsyncNan() {
  const [values, setValues] = useState([0, 0, 0, 0]);
  const [callout, setCallout] = useState(0);
  const [goal, setGoal] = useState(0);

  useEffect(() => {
    let mounted = true;

    window.setTimeout(() => {
      if (!mounted) return;
      setValues([1, 2, 3, 4]);
      setCallout(5);
      setGoal(6);
    }, 2000);

    return () => {
      mounted = false;
    };
  }, []);

  return <Racetrack values={values} goal={goal} callout={callout} />;
}

export default RacetrackAsyncNan;
