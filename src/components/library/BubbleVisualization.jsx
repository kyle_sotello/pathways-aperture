import React from 'react';
import PropTypes from 'prop-types';
import Bubble, { SecondaryBubblePosition } from '@apertures/vis-bubble';
import PieChart from '@apertures/shared/dist/components/pieChart';
import {
  BLACK,
  BLUE,
  ORANGE,
  TRANSPARENT,
  WHITE,
} from '@apertures/shared/dist/utilities/colors';

function BubbleVisualizationExample({ big, colorful }) {
  const primaryLabel = 'Some Primary Bubble Caption';
  const primarySize = big ? 500 : undefined;
  const secondaryLabel = 'OF THE 270';
  const values = [52, 0.14];
  const theme = colorful
    ? {
        background: [ORANGE.PRIMARY, BLUE.MEDIUM],
        colors: [BLUE.MEDIUM, ORANGE.PRIMARY],
        text: [BLUE.MEDIUM, ORANGE.PRIMARY],
      }
    : {};

  const renderCustomContent = (secondary = false) => (value) =>
    secondary ? (
      <PieChart step={4} theme={{ colors: [ORANGE.PRIMARY] }} />
    ) : (
      <div
        style={{
          color: ORANGE.PRIMARY,
          fontSize: 15,
          textAlign: 'center',
        }}
      >
        <h1>Some custom content</h1>
        <sub style={{ fontSize: 15 }}>{value}</sub>
      </div>
    );

  return (
    <div
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        height: '100%',
        width: '100%',
      }}
    >
      <Bubble
        labels={['Unique Accounts Statused', 'SINCE LAST MONTH']}
        primarySize={primarySize}
        suffixes={['', '%']}
        theme={{
          background: BLACK.PRIMARY,
          colors: [
            BLUE.MEDIUM,
            ORANGE.PRIMARY,
            null,
            BLUE.MEDIUM,
            ORANGE.PRIMARY,
          ],
          shadow: TRANSPARENT,
          text: [ORANGE.PRIMARY, BLUE.MEDIUM],
        }}
        values={[30432, 0.12]}
      />
      <Bubble
        hideBackground
        labels={['RELIABLE']}
        primarySize={primarySize}
        suffixes={['%']}
        theme={{ ...theme, background: BLACK.PRIMARY, shadow: WHITE.PRIMARY }}
        values={[53.567]}
      />
      <Bubble
        labels={[primaryLabel, secondaryLabel]}
        primarySize={primarySize}
        secondaryIsNotDelta
        suffixes={[null, ' hrs']}
        theme={theme}
        values={[67, 24]}
      />
      <Bubble
        labels={[primaryLabel, 'SINCE LAST YEAR']}
        prefixes={['$', '$']}
        primarySize={primarySize}
        secondaryPosition={SecondaryBubblePosition.BOTTOM_LEFT}
        theme={theme}
        values={values}
      />
      <Bubble
        primarySize={primarySize}
        renderPrimaryContent={renderCustomContent()}
        renderSecondaryContent={renderCustomContent(true)}
        theme={theme}
        values={values}
      />
      <Bubble
        labels={[primaryLabel]}
        primarySize={primarySize}
        renderSecondaryContent={renderCustomContent(true)}
        theme={theme}
        values={values}
      />
      <Bubble
        labels={[null, secondaryLabel]}
        primarySize={primarySize}
        renderPrimaryContent={renderCustomContent()}
        secondaryIsNotDelta
        suffixes={[null, '%']}
        theme={theme}
        values={[52, 34]}
      />
      <Bubble
        hasTicks
        hideBackground
        labels={[primaryLabel, 'SINCE LAST WEEK']}
        primarySize={primarySize}
        secondaryPosition={SecondaryBubblePosition.TOP_RIGHT}
        suffixes={[null, '%']}
        theme={{ ...theme, shadow: WHITE.PRIMARY }}
        values={values}
      />
    </div>
  );
}

BubbleVisualizationExample.propTypes = {
  big: PropTypes.bool,
  colorful: PropTypes.bool,
};

BubbleVisualizationExample.defaultProps = {
  big: false,
  colorful: false,
};

export default BubbleVisualizationExample;
