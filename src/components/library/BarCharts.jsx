import React from 'react';
import BarChartHorizontalGrouped from '@apertures/vis-bar-chart-horizontal-grouped';
import BarChartHorizontalStacked from '@apertures/vis-bar-chart-horizontal-stacked';
import BarChartVerticalGrouped from '@apertures/vis-bar-chart-vertical-grouped';
import BarChartVerticalStacked from '@apertures/vis-bar-chart-vertical-stacked';

const example1LegendLabels = ['Dataset 1'];
const example1MainLabels = ['Jan', 'Feb', 'March', 'April', 'May', 'June'];
const example1Values = [[12, 19, 3, 5, 2, 3]];

const example2LegendLabels = ['Dataset 1', 'Dataset 2'];
const example2MainLabels = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
];
const example2Values = [
  [123000, 193200, 32393, 529894, 229240, 33283],
  [548948, 332903, 813904, 103238, 163423, 90935],
];

const example3LegendLabels = [
  'Test Data',
  'Real Data',
  'What is This',
  'Hello World',
  'Goodbye World',
  'Hellogoodbye',
  'Another Test',
  'Hellogoodbye',
  'Another Test',
];
const example3MainLabels = [
  'Jan',
  'Feb',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'Sept',
];
const example3Values = [
  [12, 19, 3, 5, 2, 3, 8, 12, 10],
  [5, 3, 8, 10, 16, 9, 7, 13, 11],
  [2, 1, 10, 7, 6, 12, 6, 14, 12],
  [7, 9, 11, 4, 9, 4, 5, 11, 13],
  [13, 22, 18, 2, 25, 6, 4, 9, 14],
  [13, 22, 18, 2, 25, 6, 3, 12, 15],
  [5, 3, 8, 10, 16, 9, 7, 13, 14],
  [5, 3, 8, 10, 16, 9, 7, 13, 11],
  [13, 22, 18, 2, 25, 6, 4, 9, 14],
];

function BarChartsExample() {
  return (
    <>
      <h1 style={{ color: 'white' }}>Vertical</h1>
      <div style={{ display: 'flex' }}>
        <div>
          <BarChartVerticalGrouped
            legendLabels={example1LegendLabels}
            mainLabels={example1MainLabels}
            values={example1Values}
          />
          <BarChartVerticalGrouped
            legendLabels={example2LegendLabels}
            mainLabels={example2MainLabels}
            values={example2Values}
          />
          <BarChartVerticalGrouped
            legendLabels={example3LegendLabels}
            mainLabels={example3MainLabels}
            values={example3Values}
          />
        </div>
        <div>
          <BarChartVerticalStacked
            legendLabels={example1LegendLabels}
            mainLabels={example1MainLabels}
            values={example1Values}
          />
          <BarChartVerticalStacked
            legendLabels={example2LegendLabels}
            mainLabels={example2MainLabels}
            values={example2Values}
          />
          <BarChartVerticalStacked
            legendLabels={example3LegendLabels}
            mainLabels={example3MainLabels}
            values={example3Values}
          />
        </div>
      </div>
      <h1 style={{ color: 'white' }}>Horizontal</h1>
      <div style={{ display: 'flex' }}>
        <div>
          <BarChartHorizontalGrouped
            legendLabels={example1LegendLabels}
            mainLabels={example1MainLabels}
            values={example1Values}
          />
          <BarChartHorizontalGrouped
            legendLabels={example2LegendLabels}
            mainLabels={example2MainLabels}
            values={example2Values}
          />
          <BarChartHorizontalGrouped
            legendLabels={example3LegendLabels}
            mainLabels={example3MainLabels}
            values={example3Values}
          />
        </div>
        <div>
          <BarChartHorizontalStacked
            legendLabels={example1LegendLabels}
            mainLabels={example1MainLabels}
            values={example1Values}
          />
          <BarChartHorizontalStacked
            legendLabels={example2LegendLabels}
            mainLabels={example2MainLabels}
            values={example2Values}
          />
          <BarChartHorizontalStacked
            legendLabels={example3LegendLabels}
            mainLabels={example3MainLabels}
            values={example3Values}
          />
        </div>
      </div>
    </>
  );
}

export default BarChartsExample;
