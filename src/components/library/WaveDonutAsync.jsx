import React, { useEffect, useState } from 'react';
import WaveDonut from '@apertures/vis-wave-donut';

function WaveDonutAsync() {
  const [value, setValue] = useState(0.25);

  useEffect(() => {
    let mounted = true;

    window.setTimeout(() => {
      if (!mounted) return;
      setValue(0.75);

      window.setTimeout(() => {
        if (!mounted) return;
        setValue(0.5);
      }, 5000);
    }, 2000);

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <div>
      <WaveDonut values={[value, value]} />
      {value}
    </div>
  );
}

export default WaveDonutAsync;
