import React from 'react';
import SharkFin from '@apertures/vis-shark-fin';
import { PURPLE } from '@apertures/shared/dist/utilities/colors';

function SharkFinExample() {
  const props = {
    avgLabel: 'Avg. Days to Paid',
    avgValue: 10.21,
    changesLabel: 'over the last 30 days',
    changesValue: 0.035,
    labels: [
      'label',
      'long label',
      'long long label',
      'long long label',
      'long long long label',
      'long long long label',
      'long long long long label',
      'long long long long label',
      'long long long long long label',
      'long long long long long label',
    ],
    values: [50, 40, 35, 30, 25, 20, 15, 10, 5, 1],
  };

  return (
    <div>
      <SharkFin {...props} />
      <div style={{ margin: '30px' }} />
      <SharkFin {...props} reverse />
      <div style={{ margin: '30px' }} />
      <SharkFin
        {...props}
        bullet
        theme={{
          colors: [PURPLE.PRIMARY, PURPLE.DARK],
          shadow: PURPLE.BRIGHT,
        }}
      />
      <div style={{ margin: '30px' }} />
      <SharkFin
        {...props}
        bullet
        reverse
        theme={{
          colors: [PURPLE.PRIMARY, PURPLE.DARK],
          shadow: PURPLE.BRIGHT,
        }}
      />
    </div>
  );
}

export default SharkFinExample;
