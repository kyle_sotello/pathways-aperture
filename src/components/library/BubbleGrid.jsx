import React from 'react';

import BubbleGrid from '@apertures/vis-bubble-grid';

function BubbleGridVisualizationExample() {
  const labels = ['Main Label', 'Top', 'Bottom'];

  return (
    <div
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        height: '100%',
        justifyContent: 'space-evenly',
        width: '100%',
      }}
    >
      <BubbleGrid labels={labels} values={[1, 456, 789]} />
      <BubbleGrid labels={labels} values={[3, null, 789]} />
      <BubbleGrid labels={labels} values={[7, 456, null]} />
      <BubbleGrid labels={labels} values={[22, null, null]} />
      <BubbleGrid labels={labels} values={[123, 456, 789]} />
      <BubbleGrid labels={labels} values={[1234, 456, 789]} />
    </div>
  );
}

BubbleGridVisualizationExample.propTypes = {};

BubbleGridVisualizationExample.defaultProps = {};

export default BubbleGridVisualizationExample;
