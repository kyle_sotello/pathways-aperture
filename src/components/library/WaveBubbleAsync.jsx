import React, { useEffect, useState } from 'react';
import WaveBubble from '@apertures/vis-wave-bubble';

function WaveBubbleAsync() {
  const [values, setValues] = useState([1, 2]);

  useEffect(() => {
    let mounted = true;

    window.setTimeout(() => {
      if (!mounted) return;
      setValues([1, 2, 3]);

      window.setTimeout(() => {
        if (!mounted) return;
        setValues([1, 2, 3, 4]);
      }, 5000);
    }, 2000);

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <div>
      <WaveBubble values={values} aggregate />
      {values.join(',')}
    </div>
  );
}

export default WaveBubbleAsync;
