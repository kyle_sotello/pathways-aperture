import React, { useEffect, useState } from 'react';
import LedBubble from '@apertures/vis-led-bubble';

const values = [
  [0.76, 0.0802],
  [0.43, -0.12],
  [0.97, 0.5],
];

function LedBubbleAsyncExample() {
  const [valuesIdx, setValuesIdx] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setValuesIdx((prev) => (prev + 1) % values.length);
    }, 10000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return <LedBubble values={values[valuesIdx]} />;
}

export default LedBubbleAsyncExample;
