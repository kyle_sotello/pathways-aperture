import React from 'react';
import Flexbox from 'flexbox-react';
import { BLUE, ORANGE, WHITE } from '@apertures/shared/dist/utilities/colors';
import LedBubble from '@apertures/vis-led-bubble';

const awesomeTheme = {
  background: ORANGE.PRIMARY,
  colors: [ORANGE.PRIMARY, BLUE.BRIGHT],
  outline: BLUE.BRIGHT,
  shadow: ORANGE.PRIMARY,
  text: WHITE.PRIMARY,
};

function LedBubbleExample() {
  return (
    <Flexbox
      alignItems="center"
      flexDirection="column"
      justifyContent="space-around"
    >
      <LedBubble label="SUCCESSFUL" values={[0.7649, 0.0802]} />
      <LedBubble
        label="ORANGE AND BLUE"
        theme={awesomeTheme}
        values={[0.752, -0.0545]}
      />
      <LedBubble
        label="Look I don't have a second bubble"
        theme={awesomeTheme}
        values={[0.752]}
      />
      <LedBubble
        numCircles={20}
        theme={awesomeTheme}
        values={[0.583, 0.158]}
        size={300}
      />
      <LedBubble
        numCircles={100}
        label="PERCENT CHANCE OF BRIAN BECOMING A UX DESIGNER"
        theme={awesomeTheme}
        values={[0.9999, 0]}
        size={800}
      />
    </Flexbox>
  );
}

export default LedBubbleExample;
