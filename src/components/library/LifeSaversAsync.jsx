import React, { useEffect, useState } from 'react';
import LifeSavers from '@apertures/vis-life-savers';
import { GREEN } from '@apertures/shared/dist/utilities/colors';

function LifeSaversAsync() {
  const [values, setValues] = useState([0.2098, -0.0737, 0.2265]);

  useEffect(() => {
    let mounted = true;

    window.setTimeout(() => {
      if (!mounted) return;
      setValues([0.75, 0.1538, 0.65]);

      window.setTimeout(() => {
        if (!mounted) return;
        setValues([0.5, 0.25, 0.4]);
      }, 5000);
    }, 5000);

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <div style={{ background: 'black' }}>
      <LifeSavers
        colors={[GREEN.PRIMARY, GREEN.PRIMARY]}
        labels={['PRIMARY', 'SECONDARY']}
        values={values}
      />
      <div style={{ color: 'white' }}>{values.join(', ')}</div>
    </div>
  );
}

export default LifeSaversAsync;
