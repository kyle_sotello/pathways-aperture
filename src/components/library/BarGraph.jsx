import React, { useEffect, useState } from 'react';
import BarGraph from '@apertures/vis-bar-graph';

const STARTING_VALUES = [
  [
    [0, 0, 0],
    [0, 0, null],
    [0, 0],
  ],
  [
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
  ],
  [
    [0, 0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
  ],
  [
    [0, 0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
  ],
];

const ENDING_VALUES = [
  [
    [30000, 5000, 5000],
    [20000, 10000, null],
    [10000, 10000],
  ],
  [
    [40000, 10000],
    [30000, 10000],
    [20000, 10000],
    [10000, 10000],
  ],
  [
    [100000, 30000, 20000],
    [60000, 40000],
    [30000, 30000],
    [10000, 20000],
  ],
  [
    [50000, 5000, 5000],
    [40000, 10000],
    [30000, 10000],
    [20000, 10000],
    [10000, 10000],
  ],
  [
    [20000, 10000],
    [30000, 10000],
    [40000, 10000],
    [10000, 10000],
  ],
];

function BarGraphVisualizationExample() {
  const [values, setValues] = useState(STARTING_VALUES);

  useEffect(() => {
    setTimeout(() => {
      setValues(ENDING_VALUES);
    }, 5000);
  }, []);

  return (
    <div>
      <BarGraph
        activeIndex={0}
        labels={['MAR', 'FEB', 'JAN']}
        legend={['Cumulative', 'Non-cumulative', 'Forecasted']}
        step={1}
        values={values[0]}
      />
      <BarGraph
        activeIndex={2}
        labels={['APR', 'MAR', 'FEB', 'JAN']}
        legend={['Cumulative', 'Non-cumulative']}
        step={2}
        values={values[1]}
      />
      <BarGraph
        activeIndex={3}
        labels={['APR', 'MAR', 'FEB', 'JAN']}
        legend={['Cumulative', 'Non-cumulative']}
        step={3}
        values={values[2]}
      />
      <BarGraph
        activeIndex={0}
        labels={['MAY', 'APR', 'MAR', 'FEB', 'JAN']}
        legend={['Cumulative', 'Non-cumulative', 'Forecasted']}
        step={4}
        values={values[3]}
      />
      <BarGraph
        activeIndex={0}
        labels={['APR', 'MAR', 'FEB', 'JAN']}
        legend={['Cumulative', 'Non-cumulative']}
        values={values[4]}
      />
      <BarGraph legend={['Cumulative', 'Non-cumulative']} step={1} />
      <BarGraph step={1} />
      <BarGraph />
    </div>
  );
}

BarGraphVisualizationExample.propTypes = {};

BarGraphVisualizationExample.defaultProps = {};

export default BarGraphVisualizationExample;
