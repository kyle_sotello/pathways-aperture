import React, { useEffect, useState } from 'react';
import Racetrack from '@apertures/vis-olive-racetrack';

function RacetrackAsync() {
  const [values, setValues] = useState([0.1, 0.1, 0.1]);
  const [callout, setCallout] = useState(0);
  const [goal, setGoal] = useState(0);

  useEffect(() => {
    let mounted = true;

    window.setTimeout(() => {
      if (!mounted) return;
      setValues([1, 2, 3, 4]);
      setCallout(5);
      setGoal(6);

      window.setTimeout(() => {
        if (!mounted) return;
        setValues([5, 4, 3, 2]);
        setCallout(1);
      }, 10000);
    }, 2000);

    return () => {
      mounted = false;
    };
  }, []);

  return <Racetrack values={values} goal={goal} callout={callout} />;
}

export default RacetrackAsync;
