import React from 'react';
import PropTypes from 'prop-types';
import Shamrock from '@apertures/vis-shamrock';

function generateCpa() {
  return {
    quadrant: Math.floor(Math.random() * 4),
    size: Math.floor(Math.random() * 3) + 1,
    group: Math.floor(Math.random() * 5),
  };
}

const values = new Array(500).fill('').map(generateCpa);

function ShamrockNeverSettle({ big }) {
  return (
    <div
      style={{
        backgroundColor: '#651fff',
        padding: 30,
        marginLeft: big ? 1000 : 0,
        marginTop: big ? 500 : 0,
        transform: `scale(${big ? 3 : 1})`,
      }}
    >
      <Shamrock
        labels={{ primary: 'speed', secondary: 'impact' }}
        theme={{ text: '#000' }}
        values={values}
        width={400}
      />
    </div>
  );
}

ShamrockNeverSettle.propTypes = {
  big: PropTypes.bool,
};

ShamrockNeverSettle.defaultProps = {
  big: false,
};

export default ShamrockNeverSettle;
