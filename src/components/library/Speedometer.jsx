import React, { useEffect, useState } from 'react';
import Speedometer from '@apertures/vis-speedometer';

const VALUES = [
  [100000, 1000000, 0.1],
  [800000, 1000000, 0.8],
  [500000, 1000000, 0.5],
  [1000000, 1000000, 1],
  [200000, 1000000, 0.2],
  [0, 1000000, 0],
  [600000, 1000000, 0.6],
];

function SpeedometerExample() {
  const [index, setIndex] = useState(0);

  useEffect(() => {
    let mounted = true;

    const interval = window.setInterval(() => {
      if (!mounted) return;
      setIndex((prev) => (prev + 1) % VALUES.length);
    }, 8000);

    return () => {
      mounted = false;
      window.clearInterval(interval);
    };
  }, []);

  return (
    <div>
      <Speedometer values={VALUES[index]} />
    </div>
  );
}

export default SpeedometerExample;
