import React, { useEffect, useState } from 'react';
import Thermostat from '@apertures/vis-thermostat';

const VALUES = [0.1, 0.8, 0.5, 1, 0.2, 0, 0.6];

function ThermostatAsync() {
  const [index, setIndex] = useState(0);

  useEffect(() => {
    let mounted = true;

    const interval = window.setInterval(() => {
      if (!mounted) return;
      setIndex((prev) => (prev + 1) % VALUES.length);
    }, 5000);

    return () => {
      mounted = false;
      window.clearInterval(interval);
    };
  }, []);

  return (
    <div style={{ background: '#222' }}>
      <Thermostat value={VALUES[index]} label="Another Label" />
    </div>
  );
}

export default ThermostatAsync;
