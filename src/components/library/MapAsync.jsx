import React, { useEffect, useState } from 'react';
import Map from '@apertures/vis-map';

const mapPoints = [
  { label: 'AlphaSite 001', coordinates: [-118.243683, 34.052235] },
  { label: 'AlphaSite 002', coordinates: [-76.61219, 39.290386] },
  { label: 'AlphaSite 003', coordinates: [-91.251862, 43.808769] },
  { label: 'AlphaSite 004', coordinates: [-84.512016, 39.103119] },
  { label: 'AlphaSite 005', coordinates: [-80.843124, 35.227085] },
  { label: 'AlphaSite 006', coordinates: [-71.074366, 42.37032] },
  { label: 'AlphaSite 007', coordinates: [-157.853716, 21.307974] },
];

function MapAsync() {
  const [i, setI] = useState(0);
  const [length, setLength] = useState(0);

  useEffect(() => {
    let mounted = true;

    const interval = window.setInterval(() => {
      if (!mounted) return;
      setLength((prev) => (prev + 1) % mapPoints.length);
    }, 5000);

    return () => {
      mounted = false;
      window.clearInterval(interval);
    };
  }, []);

  return (
    <Map
      current={0}
      onMarkerClick={setI}
      selected={i}
      values={mapPoints.slice(0, length + 1)}
    />
  );
}

export default MapAsync;
