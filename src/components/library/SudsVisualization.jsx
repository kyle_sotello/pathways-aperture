import React from 'react';
import Suds from '@apertures/vis-suds';
import {
  BLUE,
  GREEN,
  ORANGE,
  PINK,
  PURPLE,
} from '@apertures/shared/dist/utilities/colors';

function generateRandomValues(n) {
  const randomValues = [];
  for (let i = 0; i < n; i += 1) {
    randomValues.push(Math.random());
  }
  const sum = randomValues.reduce((a, b) => a + b, 0);

  return randomValues.map((value) => value / sum);
}

function SudsVisualizationExample() {
  return (
    <div style={{ padding: 24 }}>
      <Suds hasNoKey height={200} values={[0, 0, 0]} width={300} />
      <Suds
        hasNoKey
        height={200}
        values={generateRandomValues(6)}
        width={300}
      />
      <Suds
        labels={[
          'Labor Impact',
          'Non-Labor Impact',
          'Net Revenue Yield Impact',
          'Net Revenue Growth Impact',
          'Other Impact',
          'One-Time Cash Acceleration',
        ]}
        values={[0.3, 0.2, 0.2, 0.15, 0.15]}
      />
      <Suds
        labels={[
          'Labor Impact',
          'Non-Labor Impact',
          'Net Revenue Yield Impact',
          'Net Revenue Growth Impact',
          'Other Impact',
          'One-Time Cash Acceleration',
        ]}
        values={[0.5, 0.02, 0.4, 0.03, 0.01]}
      />
      <Suds
        labels={[
          'Labor Impact',
          'Non-Labor Impact',
          'Net Revenue Yield Impact',
          'Net Revenue Growth Impact',
          'Other Impact',
          'One-Time Cash Acceleration',
        ]}
        values={[0.33, 0.33, 0.33]}
      />
      <Suds
        labels={[
          'Labor Impact',
          'Non-Labor Impact',
          'Net Revenue Yield Impact',
          'Net Revenue Growth Impact',
          'Other Impact',
          'One-Time Cash Acceleration',
        ]}
        values={[0.3, 0.2, 0.2, 0.15]}
      />
      <Suds
        theme={{
          colors: [
            PURPLE.PRIMARY,
            GREEN.MINT,
            BLUE.PRIMARY,
            BLUE.LIGHT,
            PURPLE.LIGHT,
            ORANGE.PRIMARY,
            PINK.PRIMARY,
          ],
        }}
        labels={[
          'Labor Impact',
          'Non-Labor Impact',
          'Net Revenue Yield Impact',
          'Net Revenue Growth Impact',
          'Other Impact',
          'One-Time Cash Acceleration',
          'Danger',
        ]}
        values={[0.3, 0.2, 0.2, 0.1, 0.1, 0.05, 0.05]}
      />
    </div>
  );
}

SudsVisualizationExample.propTypes = {};

SudsVisualizationExample.defaultProps = {};

export default SudsVisualizationExample;
