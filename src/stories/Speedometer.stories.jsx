import React from 'react';

import Speedometer from '../components/library/Speedometer';

export default {
  title: 'Example/Speedometer',
  component: Speedometer,
}

export const Primary = () => <Speedometer />