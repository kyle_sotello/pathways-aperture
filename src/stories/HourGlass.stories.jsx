import React from 'react';

import HourGlass from '../components/library/HourGlass';

export default {
  title: 'Example/HourGlass',
  component: HourGlass,
}

export const Primary = () => <HourGlass />