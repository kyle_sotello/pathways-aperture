import React from 'react';

import RaceTrackAsync from '../components/library/RaceTrackAsync';

export default {
  title: 'Example/RaceTrackAsync',
  component: RaceTrackAsync,
}

export const Primary = () => <RaceTrackAsync />