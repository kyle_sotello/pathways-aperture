import React from 'react';

import BarCharts from '../components/library/BarCharts';

export default {
  title: 'Example/BarCharts',
  component: BarCharts,
}

export const Primary = () => <BarCharts />