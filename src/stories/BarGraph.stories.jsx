import React from 'react';

import BarGraph from '../components/library/BarGraph';

export default {
  title: 'Example/BarGraph',
  component: BarGraph,
}

export const Primary = () => <BarGraph />