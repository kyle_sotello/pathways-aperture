import React from 'react';

import SudsVisualization from '../components/library/SudsVisualization';

export default {
  title: 'Example/SudsVisualization',
  component: SudsVisualization,
}

export const Primary = () => <SudsVisualization />