import React from 'react';

import Sort from '../components/library/Sort';

export default {
  title: 'Example/Sort',
  component: Sort,
}

export const Primary = () => <Sort />