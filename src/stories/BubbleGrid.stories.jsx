import React from 'react';

import BubbleGrid from '../components/library/BubbleGrid';

export default {
  title: 'Example/BubbleGrid',
  component: BubbleGrid,
}

export const Primary = () => <BubbleGrid />