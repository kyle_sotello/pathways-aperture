import React from 'react';

import MapAsync from '../components/library/MapAsync';

export default {
  title: 'Example/MapAsync',
  component: MapAsync,
}

export const Primary = () => <MapAsync />