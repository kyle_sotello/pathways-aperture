import React from 'react';

import WaveDonutAsync from '../components/library/WaveDonutAsync';

export default {
  title: 'Example/WaveDonutAsync',
  component: WaveDonutAsync,
}

export const Primary = () => <WaveDonutAsync />