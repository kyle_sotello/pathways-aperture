import React from 'react';

import RaceTrackAsyncNaN from '../components/library/RaceTrackAsyncNaN';

export default {
  title: 'Example/RaceTrackAsyncNaN',
  component: RaceTrackAsyncNaN,
}

export const Primary = () => <RaceTrackAsyncNaN />