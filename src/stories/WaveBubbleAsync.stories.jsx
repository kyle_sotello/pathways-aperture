import React from 'react';

import WaveBubbleAsync from '../components/library/WaveBubbleAsync';

export default {
  title: 'Example/WaveBubbleAsync',
  component: WaveBubbleAsync,
}

export const Primary = () => <WaveBubbleAsync />