import React from 'react';

import Loading from '../components/library/Loading';

export default {
  title: 'Example/Loading',
  component: Loading,
}

export const Primary = () => <Loading />