import React from 'react';

import GoogleSheetsLoader from '../components/library/GoogleSheetsLoader';

export default {
  title: 'Example/GoogleSheetsLoader',
  component: GoogleSheetsLoader,
}

export const Primary = () => <GoogleSheetsLoader />