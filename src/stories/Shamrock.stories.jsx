import React from 'react';

import Shamrock from '../components/library/Shamrock';

export default {
  title: 'Example/Shamrock',
  component: Shamrock,
}

export const Primary = () => <Shamrock />