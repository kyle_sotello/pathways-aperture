import React from 'react';

import Thermostat from '../components/library/Thermostat';

export default {
  title: 'Example/Thermostat',
  component: Thermostat,
}

export const Primary = () => <Thermostat />