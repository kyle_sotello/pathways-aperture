import './App.css';
import BarChartVerticalGrouped from '@apertures/vis-bar-chart-vertical-grouped';

function App() {

  const example1LegendLabels = ['Time at States by Department - Including After-Hours 09/01/2021 - 09/16/2021 Deaconess Cardiology PA Department'];
  const example1MainLabels = ['IN PROGRESS', 'CREATED', 'PARTIALLY DENIED'];
  const example1Values = [[0.28, 8.41, 14.19]];

  return (
    <div className="App">
      <h1>{'Time at States by Department - Including After-Hours 09/01/2021 - 09/16/2021 Deaconess Cardiology PA Department'}</h1>
      <BarChartVerticalGrouped
        legendLabels={example1LegendLabels}
        mainLabels={example1MainLabels}
        values={example1Values}
      />
    </div>
  );
}

export default App;
